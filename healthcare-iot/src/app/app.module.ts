/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { DataService } from './data.service';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { MedicalRecordComponent } from './MedicalRecord/MedicalRecord.component';
import { MedicalRecordDataComponent } from './MedicalRecordData/MedicalRecordData.component';
import { PractitionerDetailsComponent } from './PractitionerDetails/PractitionerDetails.component';
import { PatientDetailsComponent } from './PatientDetails/PatientDetails.component';

import { PractitionerComponent } from './Practitioner/Practitioner.component';
import { PatientComponent } from './Patient/Patient.component';

import { CreatePatientComponent } from './CreatePatient/CreatePatient.component';
import { CreatePractitionerComponent } from './CreatePractitioner/CreatePractitioner.component';
import { CreateRecordComponent } from './CreateRecord/CreateRecord.component';
import { GrantAccessToPatientComponent } from './GrantAccessToPatient/GrantAccessToPatient.component';
import { GrantAccessToRecordComponent } from './GrantAccessToRecord/GrantAccessToRecord.component';
import { RevokeAccessToPatientComponent } from './RevokeAccessToPatient/RevokeAccessToPatient.component';
import { RevokeAccessToRecordComponent } from './RevokeAccessToRecord/RevokeAccessToRecord.component';

  @NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MedicalRecordComponent,
    MedicalRecordDataComponent,
    PractitionerDetailsComponent,
    PatientDetailsComponent,
    PractitionerComponent,
    PatientComponent,
    CreatePatientComponent,
    CreatePractitionerComponent,
    CreateRecordComponent,
    GrantAccessToPatientComponent,
    GrantAccessToRecordComponent,
    RevokeAccessToPatientComponent,
    RevokeAccessToRecordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
