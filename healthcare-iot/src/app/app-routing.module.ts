/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

import { MedicalRecordComponent } from './MedicalRecord/MedicalRecord.component';
import { MedicalRecordDataComponent } from './MedicalRecordData/MedicalRecordData.component';
import { PractitionerDetailsComponent } from './PractitionerDetails/PractitionerDetails.component';
import { PatientDetailsComponent } from './PatientDetails/PatientDetails.component';

import { PractitionerComponent } from './Practitioner/Practitioner.component';
import { PatientComponent } from './Patient/Patient.component';

import { CreatePatientComponent } from './CreatePatient/CreatePatient.component';
import { CreatePractitionerComponent } from './CreatePractitioner/CreatePractitioner.component';
import { CreateRecordComponent } from './CreateRecord/CreateRecord.component';
import { GrantAccessToPatientComponent } from './GrantAccessToPatient/GrantAccessToPatient.component';
import { GrantAccessToRecordComponent } from './GrantAccessToRecord/GrantAccessToRecord.component';
import { RevokeAccessToPatientComponent } from './RevokeAccessToPatient/RevokeAccessToPatient.component';
import { RevokeAccessToRecordComponent } from './RevokeAccessToRecord/RevokeAccessToRecord.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'MedicalRecord', component: MedicalRecordComponent },
  { path: 'MedicalRecordData', component: MedicalRecordDataComponent },
  { path: 'PractitionerDetails', component: PractitionerDetailsComponent },
  { path: 'PatientDetails', component: PatientDetailsComponent },
  { path: 'Practitioner', component: PractitionerComponent },
  { path: 'Patient', component: PatientComponent },
  { path: 'CreatePatient', component: CreatePatientComponent },
  { path: 'CreatePractitioner', component: CreatePractitionerComponent },
  { path: 'CreateRecord', component: CreateRecordComponent },
  { path: 'GrantAccessToPatient', component: GrantAccessToPatientComponent },
  { path: 'GrantAccessToRecord', component: GrantAccessToRecordComponent },
  { path: 'RevokeAccessToPatient', component: RevokeAccessToPatientComponent },
  { path: 'RevokeAccessToRecord', component: RevokeAccessToRecordComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
 imports: [RouterModule.forRoot(routes)],
 exports: [RouterModule],
 providers: []
})
export class AppRoutingModule { }
