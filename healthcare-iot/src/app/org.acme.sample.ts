import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace org.acme.sample{
   export class MedicalRecord extends Asset {
      recordId: string;
      description: string;
      date: string;
      permissions: string[];
      owner: Patient;
      author: Practitioner;
      recordData: MedicalRecordData;
   }
   export class MedicalRecordData extends Asset {
      dataId: string;
      data: string;
      record: MedicalRecord;
   }
   export class PractitionerDetails extends Asset {
      practitionerDetailsId: string;
      name: string;
      phoneNo: string;
      owner: Practitioner;
   }
   export class PatientDetails extends Asset {
      patientDetailsId: string;
      name: string;
      phoneNo: string;
      age: string;
      height: string;
      weight: string;
      owner: Patient;
   }
   export class Practitioner extends Participant {
      practitionerId: string;
      patients: string[];
      profile: PractitionerDetails;
   }
   export class Patient extends Participant {
      patientId: string;
      authorized: string[];
      medicalRecords: MedicalRecord[];
      personalDetails: PatientDetails;
   }
   export class CreatePatient extends Transaction {
      patientId: string;
      name: string;
      phoneNo: string;
      age: string;
      height: string;
      weight: string;
   }
   export class CreatePractitioner extends Transaction {
      practitionerId: string;
      name: string;
      phoneNo: string;
   }
   export class CreateRecord extends Transaction {
      recordId: string;
      description: string;
      data: string;
      date: string;
      practitioner: Practitioner;
      patient: Patient;
   }
   export class GrantAccessToPatient extends Transaction {
      practitionerId: string;
      patient: Patient;
   }
   export class GrantAccessToRecord extends Transaction {
      practitionerId: string;
      record: MedicalRecord;
   }
   export class RevokeAccessToPatient extends Transaction {
      practitionerId: string;
      patient: Patient;
   }
   export class RevokeAccessToRecord extends Transaction {
      practitionerId: string;
      record: MedicalRecord;
   }
// }
