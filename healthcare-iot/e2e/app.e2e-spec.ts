/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AngularTestPage } from './app.po';
import { ExpectedConditions, browser, element, by } from 'protractor';
import {} from 'jasmine';


describe('Starting tests for healthcare-iot', function() {
  let page: AngularTestPage;

  beforeEach(() => {
    page = new AngularTestPage();
  });

  it('website title should be healthcare-iot', () => {
    page.navigateTo('/');
    return browser.getTitle().then((result)=>{
      expect(result).toBe('healthcare-iot');
    })
  });

  it('network-name should be healthcare-iot@1.0.1-deploy.0',() => {
    element(by.css('.network-name')).getWebElement()
    .then((webElement) => {
      return webElement.getText();
    })
    .then((txt) => {
      expect(txt).toBe('healthcare-iot@1.0.1-deploy.0.bna');
    });
  });

  it('navbar-brand should be healthcare-iot',() => {
    element(by.css('.navbar-brand')).getWebElement()
    .then((webElement) => {
      return webElement.getText();
    })
    .then((txt) => {
      expect(txt).toBe('healthcare-iot');
    });
  });

  
    it('MedicalRecord component should be loadable',() => {
      page.navigateTo('/MedicalRecord');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('MedicalRecord');
      });
    });

    it('MedicalRecord table should have 8 columns',() => {
      page.navigateTo('/MedicalRecord');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(8); // Addition of 1 for 'Action' column
      });
    });
  
    it('MedicalRecordData component should be loadable',() => {
      page.navigateTo('/MedicalRecordData');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('MedicalRecordData');
      });
    });

    it('MedicalRecordData table should have 4 columns',() => {
      page.navigateTo('/MedicalRecordData');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(4); // Addition of 1 for 'Action' column
      });
    });
  
    it('PractitionerDetails component should be loadable',() => {
      page.navigateTo('/PractitionerDetails');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('PractitionerDetails');
      });
    });

    it('PractitionerDetails table should have 5 columns',() => {
      page.navigateTo('/PractitionerDetails');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(5); // Addition of 1 for 'Action' column
      });
    });
  
    it('PatientDetails component should be loadable',() => {
      page.navigateTo('/PatientDetails');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('PatientDetails');
      });
    });

    it('PatientDetails table should have 8 columns',() => {
      page.navigateTo('/PatientDetails');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(8); // Addition of 1 for 'Action' column
      });
    });
  

  
    it('Practitioner component should be loadable',() => {
      page.navigateTo('/Practitioner');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Practitioner');
      });
    });

    it('Practitioner table should have 4 columns',() => {
      page.navigateTo('/Practitioner');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(4); // Addition of 1 for 'Action' column
      });
    });
  
    it('Patient component should be loadable',() => {
      page.navigateTo('/Patient');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Patient');
      });
    });

    it('Patient table should have 5 columns',() => {
      page.navigateTo('/Patient');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(5); // Addition of 1 for 'Action' column
      });
    });
  

  
    it('CreatePatient component should be loadable',() => {
      page.navigateTo('/CreatePatient');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('CreatePatient');
      });
    });
  
    it('CreatePractitioner component should be loadable',() => {
      page.navigateTo('/CreatePractitioner');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('CreatePractitioner');
      });
    });
  
    it('CreateRecord component should be loadable',() => {
      page.navigateTo('/CreateRecord');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('CreateRecord');
      });
    });
  
    it('GrantAccessToPatient component should be loadable',() => {
      page.navigateTo('/GrantAccessToPatient');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('GrantAccessToPatient');
      });
    });
  
    it('GrantAccessToRecord component should be loadable',() => {
      page.navigateTo('/GrantAccessToRecord');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('GrantAccessToRecord');
      });
    });
  
    it('RevokeAccessToPatient component should be loadable',() => {
      page.navigateTo('/RevokeAccessToPatient');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('RevokeAccessToPatient');
      });
    });
  
    it('RevokeAccessToRecord component should be loadable',() => {
      page.navigateTo('/RevokeAccessToRecord');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('RevokeAccessToRecord');
      });
    });
  

});