{
  "$class": "org.acme.sample.CreatePatient",
  "patientId": "1",
  "name": "Vamsi",
  "phoneNo": "9130605918",
  "age": "24",
  "height": "184",
  "weight": "67"
}
{
  "$class": "org.acme.sample.CreatePractitioner",
  "practitionerId": "1",
  "name": "Krishna",
  "phoneNo": "999999999"
}

{
	"$class": "org.acme.sample.CreateRecord",
	"recordId": "1",
	"description": "Fever",
	"data": "[{\"day\":1,\"pulseRate\":\"125\",\"reportDate\":\"2018-06-14T18:30:00.000Z\",\"bloodPressure\":\"25\",\"bloodSugarLevel\":\"32\",\"weight\":\"45\",\"medicine1\":\"Asprin\",\"medicine2\":\"Crocin\"}]",
	"date": "2018-07-06",
	"practitioner": "resource:org.acme.sample.Practitioner#1",
	"patient": "resource:org.acme.sample.Patient#1"
}